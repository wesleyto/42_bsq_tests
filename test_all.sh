### NORMINETTE ###
if ([ "$#" -eq 1 ] && [ $1 == '-n' ]);
then
	echo ===========================
	echo === Skipping Norminette ===
	echo ===========================
else
	echo ===========================
	echo ======= Norminette ========
	echo ===========================
	norminette *.c *.h
fi
echo ...Done
echo

### COMPILING FILES ###
echo ===========================
echo ===== Test Compiling ======
echo ===========================
gcc -Wall -Wextra -Werror -c *.c
rm *.o
echo ...Done
echo

### COPY OVER TEST FILES ###
echo ===========================
echo ====== Copying Files ======
echo ===========================
cp * TESTFILES/
echo ...Done
echo

### COMPILE WITH TEST FILES ###
echo ===========================
echo ===== Compiling Mains =====
echo ===========================
cd TESTFILES
make re
make clean
echo ...Done
echo

### RUN TEST FILES ###
echo ===========================
echo ==== Testing Examples =====
echo ===========================
for f in example*
do
	echo =====================================
	echo == $f
	echo =====================================
	./bsq $f
	echo
done

read -p "Press [Enter] key to run multiple files..."

### RUN ALL FILES ###
echo ===========================
echo ======= Testing All =======
echo ===========================
./bsq example*

read -p "Press [Enter] key to run invalid files..."

### TESTING INVALID FILES ###
echo ===========================
echo ===== Testing Invalid =====
echo ===========================
./bsq invalid1 invalid2 invalid3

read -p "Press [Enter] key to run mixed files..."

### TESTING VALID & INVALID FILES ###
echo ===========================
echo ====== Testing Mixed ======
echo ===========================
./bsq example00* invalid example27*

read -p "Press [Enter] key to run STDIN..."

### TESTING STDIN ###
echo ===========================
echo ====== Testing StdIn ======
echo ===========================
cat example31* | ./bsq
echo
echo "3.ox\n..o\n..o\noo." | ./bsq

### TESTING Evals ###
for num in $( seq -f "%02g" 0 10 )
do
read -p "Press [Enter] key to run Eval $num..."

	echo ===========================
	echo ===== Testing Eval $num =====
	echo ===========================
	./bsq eval$num*
done

make fclean
cd ..
cd ..