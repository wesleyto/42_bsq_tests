### What is this repository for? ###

* Basic testing of 42 BSQ

### How do I get set up? ###

* Copy/download/clone to your project directory. Run the script.

### Who do I talk to? ###

* Repo owner or admin
* A deity / spiritual being / therapist of your choice.

### Thanks ###
* Angus Yip for posting some of these tests on Slack.